#################################################
#                 Timecube Chess                #
#################################################
Author: William Bennett
A simple, 2d implementation of a chess variant known as timecube chess (http://1d4chan.org/wiki/Timecube_Chess)

To play, simply run main.py.

Dependencies: Python 3.3.4 or higher, pygame 1.9.2a0 or higher. Possibly Windows, I don't have a Mac/Linux machine to test against.