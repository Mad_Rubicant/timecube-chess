import pygame

pygame.font.init()
class MyFont:
    def __init__(self, size):
        self.font = pygame.font.SysFont("Courier", size, bold=1)

    def change_size(self, size):
        self.font = pygame.font.SysFont("Courier", size, bold=1)

    def toggle_bold(self):
        if self.font.get_bold():
            self.font.set_bold(0)
        else:
            self.font.set_bold(1)

    def center(self, screen):
        return screen.get_width()//2

    def print(self, text, screen, rect, **kwargs):
        """Text is your message. Screen is the target surface.
        Origin is the top left corner of where you want it to draw
        kwargs are: centered: Put the text in the center of the surface
        reveresed: Flip the background color and the text color"""
        x = rect[0]
        y = rect[1]
        size = self.font.size(text)
        #background = (250, 250, 250)
        background = None
        color = (0, 0, 0)
        antialiasing = False
        if kwargs is not None:
            if "centered" in kwargs:
                if kwargs["centered"]:
                    # Overwrites left border to center
                    x = self.center(screen)
                    x -= size[0]//2

            if "centy" in kwargs:
                if kwargs["centy"]:
                    y = self.center(screen)
                    y -= size[0]//2
            if "reversed" in kwargs:
                if kwargs["reversed"]:
                    background = (250, 250, 250)
                    background, color = color, background

            if  "aa" in kwargs:
                if kwargs["aa"]:
                    antialiasing = True
        font = self.font.render(text, antialiasing, color, background)
        screen.blit(font, (x, y))