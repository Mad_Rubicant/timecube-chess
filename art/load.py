import pygame, os.path

class Image:
    def __init__(self, filename):
        long_path = os.path.join("art", filename)
        self.image = pygame.image.load(long_path)
        self.rect = self.image.get_rect()

def load_piece_images():
    black = ("black_bishop.png", "black_king.png", "black_knight.png", "black_pawn.png", "black_queen.png",
             "black_rook.png")
    white = ("white_bishop.png", "white_king.png", "white_knight.png", "white_pawn.png", "white_queen.png",
             "white_rook.png")
    images = [black, white]
    #for i in range(6):
    #    images[0] += [Image(black[i])]
    #for i in range(6):
    #    images[1] += [Image(white[i])]
    return images

def load_blank():
    image = Image("no_piece.png")
    return image

def load_chessboard():
    chessboard = Image("chessboard.gif")
    return chessboard