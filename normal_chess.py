import pygame, sys
from objects import chessboard, chess_piece, player, misc
from objects.coordinate import Coordinate as Coord
from scripts import setup_pieces
import art.load


class Chess:
    def __init__(self, screen):
        self.board = chessboard.Chessboard(art.load.load_chessboard())
        #self.player_one = player.Player(0)
        #self.player_two = player.Player(1)
        #self.player_focus = self.player_one
        self.white_turn = True
        self.selection = None
        self.screen = screen
        self.clock = pygame.time.Clock()
        self.counter = misc.TurnCounter()
        self.marker = misc.TurnMarker()

    # def switch_turn(self):
    #     if self.player_focus == self.player_one:
    #         self.player_focus = self.player_two
    #     else:
    #         self.player_focus = self.player_one

    def switch_turn(self):
        self.white_turn = not self.white_turn
        self.marker.flip()
        self.counter.increment()

    def play_chess(self):
        screen = self.screen
        board = self.board
        screen.fill((255, 255, 255))
        board.center(screen)
        self.counter.rect.move_ip(board.rect.left - self.counter.rect.right -5, 40)
        self.marker.rect.move_ip(board.rect.left - self.marker.rect.right - 5, 200)
        pieces = setup_pieces.initialize_pieces()
        board.populate(pieces)
        board.draw(screen)
        highlighted = board.contents[(4, 3)]
        pygame.time.wait(200)
        clock = pygame.time.Clock()
        playing = True
        while playing:
            board.draw(screen)
            self.counter.draw(screen)
            self.marker.draw(screen)
            pygame.draw.rect(screen, (255, 0, 0), highlighted.rect.inflate(15, 15).move(-1, -1), 1)
            pygame.display.flip()
            events = pygame.event.get()
            for event in events:
            #for event in pygame.event.get((pygame.QUIT, pygame.KEYDOWN)):
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    keys = pygame.key.get_pressed()
                    if keys[pygame.K_ESCAPE]:
                        playing = False
                        break  # Go back to main menu
                    if keys[pygame.K_UP]:
                        if highlighted.position[1] == 1:
                            pass
                        else:
                            highlighted = board.contents[highlighted.position[0], highlighted.position[1] - 1]
                    if keys[pygame.K_DOWN]:
                        if highlighted.position[1] == 8:
                            pass
                        else:
                            highlighted = board.contents[highlighted.position[0], highlighted.position[1] + 1]
                    if keys[pygame.K_LEFT]:
                        if highlighted.position[0] == 1:
                            pass
                        else:
                            highlighted = board.contents[highlighted.position[0] - 1, highlighted.position[1]]
                    if keys[pygame.K_RIGHT]:
                        if highlighted.position[0] == 8:
                            pass
                        else:
                            highlighted = board.contents[highlighted.position[0] + 1, highlighted.position[1]]
                    if keys[pygame.K_RETURN]:
                        pygame.time.wait(200)
                        if highlighted.is_white() == self.white_turn:
                            self.take_move(highlighted)
                        else: continue
                        pygame.time.wait(200)
            pygame.event.pump()
            clock.tick(60)
        board.depopulate()

    def hilight(self, piece, color, bold=False):
        # Color should be a color tuple
        if bold:
            pygame.draw.rect(self.screen, color, piece.rect.inflate(15, 15).move(-1, -1), 3)
        else:
            pygame.draw.rect(self.screen, color, piece.rect.inflate(15, 15).move(-1, -1), 1)

    def take_move(self, selection):
        self.selection = selection
        moves = selection.get_moves(self.board)
        legal = True
        moved = False
        if not moves:
            legal = False
        else:
            choice = moves[0]
        pygame.time.wait(200)
        while legal:
            self.board.draw(self.screen)
            self.hilight(self.selection, (255, 0, 0))
            for move in moves:
                self.hilight(self.board.contents[move], (0, 122, 122))
            self.hilight(self.board.contents[choice], (0, 0, 255), True)
            for event in pygame.event.get([pygame.QUIT, pygame.KEYDOWN]):
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    keys = pygame.key.get_pressed()
                    if keys[pygame.K_ESCAPE]:
                        legal = False

                    if keys[pygame.K_LEFT]:
                        choice = Coord(choice.x - 1, choice.y)
                        if choice.x < 1:
                            choice = Coord(choice.x + 1, choice.y)
                    if keys[pygame.K_RIGHT]:
                        choice = Coord(choice.x + 1, choice.y)
                        if choice.x > 8:
                            choice = Coord(choice.x - 1, choice.y)
                    if keys[pygame.K_DOWN]:
                        choice = Coord(choice.x, choice.y + 1)
                        if choice.y > 8:
                            choice = Coord(choice.x, choice.y - 1)
                    if keys[pygame.K_UP]:
                        choice = Coord(choice.x, choice.y - 1)
                        if choice.y < 1:
                            choice = Coord(choice.x, choice.y + 1)

                    if keys[pygame.K_RETURN]:
                        #if choice in moves:
                            self.move(selection.get_position(), choice)
                            pygame.time.wait(200)
                            moved = True

            pygame.display.flip()
            pygame.event.pump()
            if moved:
                break
            self.clock.tick(60)

    def move(self, selection: Coord, target: Coord) -> None:
        moving_piece = self.board[selection]
        blank = setup_pieces.new_blank(selection)
        if self.board.occupied(target):
            # Move normally. This isn't an en passant
            self.board[target] = moving_piece
            moving_piece.position = target
            self.board[selection] = blank
        elif moving_piece.is_pawn() and not self.board.occupied(target):
            # This is en passant
            self.board[target] = moving_piece
            moving_piece.position = target
            self.board[selection] = blank
            if moving_piece.is_white():
                # En passant specific code
                # Piece above it gets killed
                position = Coord(target.x, target.y - 1)
                if not self.board[position].is_white():
                    # This makes sure you're not killing your own piece
                    blank = setup_pieces.new_blank(position)
                    self.board[position] = blank
            else:
                # En passant specific code
                # Piece below it gets killed
                position = Coord(target.x, target.y + 1)
                if self.board[position].is_white():
                    blank = setup_pieces.new_blank(position)
                    self.board[position] = blank
        self.board.set_piece_rects()
        self.switch_turn()

        # board[target] = self
        # temp = self.get_position()
        # self.position = target
        # board[temp] = NoPiece()
