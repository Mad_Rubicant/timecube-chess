import art.load, os, pygame, sys
import text, art.load, scripts.setup_pieces, normal_chess
from objects import chessboard
from objects import chess_piece

def play_chess():
    screen.fill((255, 255, 255))
    board = chessboard.Chessboard(art.load.load_chessboard())
    board.center(screen)
    pieces = scripts.setup_pieces.initialize_pieces()
    board.populate(pieces)
    board.draw(screen)
    time_elapsed = 751
    highlighted = board.contents[(4, 3)]
    game = normal_chess.Chess(board, screen)
    pygame.time.wait(200)
    clock = pygame.time.Clock()
    playing = True
    while playing:
        board.draw(screen)
        pygame.draw.rect(screen, (255, 0, 0), highlighted.rect.inflate(15, 15).move(-1, -1), 1)
        pygame.display.flip()
        events = pygame.event.get()
        for event in events:
        #for event in pygame.event.get((pygame.QUIT, pygame.KEYDOWN)):
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                keys = pygame.key.get_pressed()
                if keys[pygame.K_ESCAPE]:
                    playing = False
                    break # Go back to main menu
                if keys[pygame.K_UP]:
                    if highlighted.position[1] == 1:
                        pass
                    else:
                        highlighted = board.contents[highlighted.position[0], highlighted.position[1] - 1]
                if keys[pygame.K_DOWN]:
                    if highlighted.position[1] == 8:
                        pass
                    else:
                        highlighted = board.contents[highlighted.position[0], highlighted.position[1] + 1]
                if keys[pygame.K_LEFT]:
                    if highlighted.position[0] == 1:
                        pass
                    else:
                        highlighted = board.contents[highlighted.position[0] - 1, highlighted.position[1]]
                if keys[pygame.K_RIGHT]:
                    if highlighted.position[0] == 8:
                        pass
                    else:
                        highlighted = board.contents[highlighted.position[0] + 1, highlighted.position[1]]
                if keys[pygame.K_RETURN]:
                    pygame.time.wait(200)
                    game.take_move(highlighted)
                    pygame.time.wait(200)
        pygame.event.pump()
        clock.tick(60)
    board.depopulate()

def main():
    global screen
    pygame.init()
    screen_size = screen_width, screen_height = pygame.display.Info().current_w - 100, pygame.display.Info().current_h
    # screen = pygame.display.set_mode(screen_size, pygame.FULLSCREEN|pygame.HWACCEL|pygame.DOUBLEBUF)
    screen = pygame.display.set_mode((900, 700), pygame.HWACCEL|pygame.DOUBLEBUF)
    screen.fill((250, 250, 250))
    pygame.key.set_repeat(250, 100)

    font = text.MyFont(30)
    choices = ("NORMAL CHESS","TIMECUBE CHESS", "QUIT")
    # Initializing loop variables
    selection = 0
    clock = pygame.time.Clock()
    time_elapsed = 751
    while True:
        time_elapsed += clock.tick()
        screen.fill((250, 250, 250))
        font.change_size(30)
        font.print("TIMECUBE CHESS", screen, (0, 100), centered=True)
        font.change_size(18)
        for i in range(len(choices)):
            font.print(choices[i], screen, (0, 250 + 50*i), centered=True)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            selection -= 1
            if selection < 0:
                selection = 0
            time_elapsed = 751
            pygame.time.wait(125)
        if keys[pygame.K_DOWN]:
            selection += 1
            if selection >= len(choices):
                selection = len(choices) - 1
            time_elapsed = 751
            pygame.time.wait(125)

        if keys[pygame.K_RETURN]:
            pygame.time.wait(40)
            if selection == 0:
                game = normal_chess.Chess(screen)
                game.play_chess()
            elif selection == 1:
                pass
            elif selection == 2:
                pygame.quit()
                sys.exit()

        for event in pygame.event.get((pygame.QUIT, pygame.MOUSEBUTTONDOWN, pygame.MOUSEMOTION, pygame.KEYDOWN)):
            #if event.type == pygame.KEYDOWN:
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                keys = pygame.mouse.get_pressed()
                if keys[0]:
                    position = pygame.mouse.get_pos()
        if time_elapsed > 750:
            font.print(choices[selection], screen, (0, 250 + 50*selection),centered=True, reversed=True)
        if time_elapsed > 1500:
            time_elapsed = 0
        pygame.display.flip()
        pygame.event.pump()



if __name__ == "__main__":
    main()