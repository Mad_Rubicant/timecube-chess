class Player:
    def __init__(self, player_number):
        """player_number must be 0 or 1. 0 is white, 1 is black"""
        self.player_number = player_number

    def is_white(self):
        if self.player_number == 0:
            return True
        else:
            return False

