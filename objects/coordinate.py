import collections.abc

class Coordinate(collections.abc.Hashable, collections.abc.Container):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __contains__(self, item):
        if item == self.x or item == self.y:
            return True
        return False

    def __hash__(self):
        return hash((self.x, self.y))

    def __len__(self):
        return 2

    def __getitem__(self, item):
        if item == 0:
            return self.x
        elif item == 1:
            return self.y
        else: raise IndexError

    def __repr__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    def __setitem__(self, key, value):
        raise TypeError("Coordinate object does not support item assignment")

    def __delitem__(self, key):
        raise TypeError("Coordinate object does not support item deletion")

    def __eq__(self, other):
        if type(other) != tuple and type(other) != Coordinate:
            return False
        elif len(other) != 2:
            return False
        elif other[0] == self.x and other[1] == self.y:
            return True
        else: return False

    def count_right(self, x_stop=9):
        counted = []
        for i in range(self.x, x_stop):
            counted.append(Coordinate(i, self.y))
        #if self in counted:
        #    counted.remove(self)
        #print(counted)
        return counted

    def count_left(self, x_stop=0):
        counted = []
        for i in range(self.x, x_stop, -1):
            counted.append(Coordinate(i, self.y))
        return counted

    def count_up(self, y_stop=0):
        counted = []
        for i in range(self.y, y_stop, -1):
            counted.append(Coordinate(self.x, i))
        return counted

    def count_down(self, y_stop=9):
        counted = []
        for i in range(self.y, y_stop):
            counted.append(Coordinate(self.x, i))
        return counted

    def count_up_left(self, x_stop=0, y_stop=0):
        counted = []
        x = self.x
        y = self.y
        while x > x_stop and y > y_stop:
            counted.append(Coordinate(x, y))
            x -= 1
            y -= 1
        return counted

    def count_up_right(self, x_stop=9, y_stop=0):
        counted = []
        x = self.x
        y = self.y
        while x < x_stop and y > y_stop:
            counted.append(Coordinate(x, y))
            x += 1
            y -= 1
        return counted

    def count_down_left(self, x_stop=0, y_stop=9):
        counted = []
        x = self.x
        y = self.y
        while x > x_stop and y < y_stop:
            counted.append(Coordinate(x, y))
            x -= 1
            y += 1
        return counted

    def count_down_right(self, x_stop=9, y_stop=9):
        counted = []
        x = self.x
        y = self.y
        while x < x_stop and y < y_stop:
            counted.append(Coordinate(x, y))
            x += 1
            y += 1
        return counted

    def is_adjacent(self, other):
        """Checks for cardinal and diagonal adjacency"""
        if self.x == other.x:
            if self.y == other.y + 1 or self.y == other.y - 1:
                return True
                # Cardinal directions
            return False
        elif self.y == other.y:
            if self.x == other.x + 1 or self.y == other.y - 1:
                return True
            return False
        elif self.x == other.x + 1 or self.x == other.x - 1:
            if self.y == other.y + 1 or self.y == other.y - 1:
                return True
            return False
        return False