"""This file is for various small objects that don't deserve their own file."""
import pygame
import text


class TurnCounter:
    def __init__(self):
        self.image = pygame.Surface((100, 65), pygame.HWSURFACE)
        self.rect = self.image.get_rect()
        self.image.fill((255, 255, 255))
        pygame.draw.rect(self.image, (0, 0, 0), self.image.get_rect(), 3)
        self.text_renderer = text.MyFont(40)
        self.count = 0
        self.text_renderer.print(str(self.count), self.image, (10, 10), centered=True, aa=True)

    def increment(self):
        self.count += 1
        self.image.fill((255, 255, 255))
        pygame.draw.rect(self.image, (0, 0, 0), self.image.get_rect(), 3)
        self.text_renderer.print(str(self.count), self.image, (10, 10), centered=True, aa=True)

    def draw(self, screen):
        screen.blit(self.image, self.rect)

class TurnMarker:
    def __init__(self):
        self.image = pygame.Surface((50, 50), pygame.HWSURFACE)
        self.rect = self.image.get_rect()
        self.white = True
        self.image.fill((255, 255, 255))
        pygame.draw.rect(self.image, (0, 0, 0), self.rect, 3)

    def flip(self):
        if self.white:
            self.image.fill((0, 0, 0))
            pygame.draw.rect(self.image, (0, 0, 0), self.image.get_rect(), 3)
            self.white = False
        else:
            self.image.fill((255, 255, 255))
            pygame.draw.rect(self.image, (0, 0, 0), self.image.get_rect(), 3)
            self.white = True

    def draw(self, screen):
        screen.blit(self.image, self.rect)