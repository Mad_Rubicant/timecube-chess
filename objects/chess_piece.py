from objects.coordinate import Coordinate as Coord
class ChessPiece:
    def is_pawn(self):
        if type(self) == Pawn:
            return True

    def is_rook(self):
        if type(self) == Rook:
            return True
        return False

    def is_knight(self):
        if type(self) == Knight:
            return True
        return False

    def is_bishop(self):
        if type(self) == Bishop:
            return True
        return False

    def is_queen(self):
        if type(self) == Queen:
            return True
        return False

    def is_king(self):
        if type(self) == King:
            return True
        return False

    def is_none(self):
        if type(self) == NoPiece:
            return True

    def is_white(self):
        if self.color == 0:
            return False
        elif self.color is None:
            return None
        else:
            return True

    def __repr__(self):
        if self.is_bishop():
            return "Bishop"
        elif self.is_king():
            return "King"
        elif self.is_knight():
            return "Knight"
        elif self.is_pawn():
            return "Pawn"
        elif self.is_queen():
            return "Queen"
        elif self.is_rook():
            return "Rook"
        elif self.is_none():
            return "Blank Square"

    def move_rect(self, dx, dy):
        self.rect.move(dx, dy)

    def get_moves(self, board):
        moves = []
        return moves
        # To be expanded later

    def get_position(self):
        return self.position

    def get_x(self):
        return self.position[0]

    def get_y(self):
        return self.position[1]

    def capture(self, board, target):
        if self.is_white() and target.is_white():
            pass
        elif not self.is_white() and not target.is_white():
            pass
        else:
            pass
        # Also a work in progress

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Pawn(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None
        self.has_moved = False
        self.first_move = False

    def get_moves(self, board):
        if self.is_white():
            if self.has_moved:
                moves = [Coord(self.get_x(), self.get_y() + 1)]
                self.first_move = False
            else:
                moves = [Coord(self.get_x(), self.get_y() + 1), Coord(self.get_x(), self.get_y() + 2)]
                self.has_moved = True
                self.first_move = True
        else:
            if self.has_moved:
                moves = [Coord(self.get_x(), self.get_y() - 1)]
                self.first_move = False
            else:
                moves = [Coord(self.get_x(), self.get_y() - 1), Coord(self.get_x(), self.get_y() - 2)]
                self.has_moved = True
                self.first_move = True
        attacks = []
        for move in moves:
            if move[0] < 1 or move[0] > 8:
                moves.remove(move)
            elif move[1] < 1 or move[1] > 8:
                moves.remove(move)
            elif board.occupied(move):
                moves.remove(move)
                if abs(move[1] - self.position[1]) == 1:
                    attacks.append(Coord(move.x - 1, move.y))
                    attacks.append(Coord(move.x + 1, move.y))
                    for attack in attacks:
                        if attack.x < 0 or attack.y < 0 or attack.x > 8 or attack.y > 8:
                            attacks.remove(attack)
                        elif board.occupied(attack):
                            pass
                        else:
                            attacks.remove(attack)
        # This is the en passant code
        try:
            if board.occupied((Coord(self.position.x + 1, self.position.y))):
                if self.is_white() == board[(self.position.x + 1, self.position.y)].is_white():
                    pass  # Do nothing because it's your own piece
                elif not board[(self.position.x + 1, self.position.y)].is_pawn():
                    pass # Do nothing because that piece isn't a pawn
                else:
                    if self.is_white():
                        attacks.append(Coord(self.position.x + 1, self.position.y + 1))
                    else:
                        attacks.append(Coord(self.position.x + 1, self.position.y - 1))
                    # Add the en passant move
        except KeyError:
            pass  # This is a hack to prevent an entire code restructure.

        try:
            if board.occupied((Coord(self.position.x - 1, self.position.y))):
                if self.is_white() == board[(self.position.x - 1, self.position.y)].is_white():
                    pass  # Do nothing because it's your own piece
                elif not board[(self.position.x - 1, self.position.y)].is_pawn():
                    pass # Do nothing because that piece isn't a pawn
                else:
                    if self.is_white():
                        attacks.append(Coord(self.position.x - 1, self.position.y + 1))
                    else:
                        attacks.append(Coord(self.position.x - 1, self.position.y - 1))
                    # Add the en passant move
        except KeyError:
            pass  # More hacks!


        moves += attacks
        return moves

class Rook(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None
        self.has_moved = False

    def get_moves(self, board):
        moves = []
        moves += self.position.count_up()
        moves += self.position.count_down()
        moves += self.position.count_left()
        moves += self.position.count_right()
        attacks = []
        illegal_attacks = []
        # This loop removes moves that are blocked by another piece, pieces that you control
        # It adds back in legal attacks
        for move in moves.copy():
            if board.occupied(move):
                if board[move].is_white() != self.is_white():
                    if self.position in move.count_down():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up()
                    elif self.position in move.count_up():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down()
                    elif self.position in move.count_left() and move in moves:
                        if move not in illegal_attacks:
                            attacks.append(move)
                            illegal_attacks += move.count_right()
                    elif self.position in move.count_right() and move in moves:
                        if move not in illegal_attacks:
                            attacks.append(move)
                            illegal_attacks += move.count_left()


                if move.x < self.position.x:
                    for mov in move.count_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x:
                    for mov in move.count_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move.y < self.position.y:
                    for mov in move.count_up():
                        if mov in moves:
                            moves.remove(mov)
                elif move.y > self.position.y:
                    for mov in move.count_down():
                        if mov in moves:
                            moves.remove(mov)
                elif move == self.position:
                    moves.remove(move)


        moves += attacks
        return moves


class Knight(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None

    def get_moves(self, board):
        moves = []
        pos= x, y =  self.get_x(), self.get_y()
        moves.append(Coord(x - 1, y - 2))
        moves.append(Coord(x - 1, y + 2))
        moves.append(Coord(x + 1, y - 2))
        moves.append(Coord(x + 1, y + 2))
        moves.append(Coord(x - 2, y - 1))
        moves.append(Coord(x - 2, y + 1))
        moves.append(Coord(x + 2, y - 1))
        moves.append(Coord(x + 2, y + 1))
        move_copy = moves.copy()
        for move in move_copy:
            if move[0] < 1 or move[1] < 1:
                moves.remove(move)
            elif move[0] > 8 or move[1] > 8:
                moves.remove(move)
            elif board.occupied(move):
                if board[move].is_white() == self.is_white():
                    moves.remove(move)
        return moves

class Bishop(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None

    def get_moves(self, board):
        moves = []
        moves += self.position.count_up_left()
        moves += self.position.count_up_right()
        moves += self.position.count_down_left()
        moves += self.position.count_down_right()
        attacks = []
        illegal_attacks = []
        for move in moves.copy():
            if move[0] < 1 or move[1] < 1:
                moves.remove(move)
            elif move[0] > 8 or move[1] > 8:
                moves.remove(move)
            elif board.occupied(move):
                if board[move].is_white() != self.is_white():
                    if self.position in move.count_up_left():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down_right()
                    elif self.position in move.count_up_right():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down_left()
                    elif self.position in move.count_down_left():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up_right()
                    elif self.position in move.count_down_right():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up_left()

                if move.x < self.position.x and move.y < self.position.y:
                    # Squares that are up-left to the piece
                    for mov in move.count_up_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x < self.position.x and move.y > self.position.y:
                    # Down and left
                    for mov in move.count_down_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x and move.y < self.position.y:
                    # Up and right
                    for mov in move.count_up_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x and move.y > self.position.y:
                    # Down and right
                    for mov in move.count_down_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move == self.position:
                    moves.remove(move)

        moves += attacks
        return moves

class Queen(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None

    def get_moves(self, board):
        moves = []
        moves += self.position.count_up()
        moves += self.position.count_down()
        moves += self.position.count_left()
        moves += self.position.count_right()
        moves += self.position.count_up_left()
        moves += self.position.count_up_right()
        moves += self.position.count_down_left()
        moves += self.position.count_down_right()
            # Literally just the Rook and Bishop get_moves() stapled together
        illegal_attacks = []
        attacks = []
        for move in moves.copy():
            if move[0] < 1 or move[1] < 1:
                moves.remove(move)
            elif move[0] > 8 or move[1] > 8:
                moves.remove(move)
            elif board.occupied(move):
                if board[move].is_white() != self.is_white():
                    if self.position in move.count_down():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up()
                    elif self.position in move.count_up():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down()
                    elif self.position in move.count_left() and move in moves:
                        if move not in illegal_attacks:
                            attacks.append(move)
                            illegal_attacks += move.count_right()
                    elif self.position in move.count_right() and move in moves:
                        if move not in illegal_attacks:
                            attacks.append(move)
                            illegal_attacks += move.count_left()
                    elif self.position in move.count_up_left():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down_right()
                    elif self.position in move.count_up_right():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_down_left()
                    elif self.position in move.count_down_left():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up_right()
                    elif self.position in move.count_down_right():
                        if move not in illegal_attacks and move in moves:
                            attacks.append(move)
                            illegal_attacks += move.count_up_left()

                # This chain removes moves that are blocked by pieces
                if move.x < self.position.x and move.y == self.position.y:
                    # Left
                    for mov in move.count_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x and move.y == self.position.y:
                    # Right
                    for mov in move.count_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move.y < self.position.y and move.x == self.position.x:
                    # Up
                    for mov in move.count_up():
                        if mov in moves:
                            moves.remove(mov)
                elif move.y > self.position.y and move.x == self.position.x:
                    # Down
                    for mov in move.count_down():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x < self.position.x and move.y < self.position.y:
                    # Squares that are up-left to the piece
                    for mov in move.count_up_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x < self.position.x and move.y > self.position.y:
                    # Down and left
                    for mov in move.count_down_left():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x and move.y < self.position.y:
                    # Up and right
                    for mov in move.count_up_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move.x > self.position.x and move.y > self.position.y:
                    # Down and right
                    for mov in move.count_down_right():
                        if mov in moves:
                            moves.remove(mov)
                elif move == self.position:
                    moves.remove(move)
        moves += attacks
        return moves

class King(ChessPiece):
    def __init__(self, color, image):
        """Color must be 0 (white) or 1 (black)"""
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None
        self.has_moved = False

    def get_moves(self, board):
        moves = []
        x, y = self.get_x(), self.get_y()
        moves.append(Coord(x - 1, y - 1))
        moves.append(Coord(x - 1, y + 1))
        moves.append(Coord(x + 1, y + 1))
        moves.append(Coord(x + 1, y - 1))
        moves.append(Coord(x, y - 1))
        moves.append(Coord(x, y + 1))
        moves.append(Coord(x - 1, y))
        moves.append(Coord(x + 1, y))

        for move in moves.copy():
            if move[0] < 1 or move[1] < 1:
                moves.remove(move)
            elif move[0] > 8 or move[1] > 8:
                moves.remove(move)
            elif board.occupied(move):
                if self.is_white() == board[move].is_white():
                    moves.remove(move)
        if self.is_white():
            for piece in board.black_pieces:
                opponent_moves = piece.get_moves(board)
                for move in opponent_moves:
                    if move in moves:
                        moves.remove(move)
                del opponent_moves
                if moves is None:
                    break
        elif not self.is_white():
            for piece in board.white_pieces:
                opponent_moves = piece.get_moves(board)
                for move in opponent_moves:
                    if move in moves:
                        moves.remove(move)
                del opponent_moves
                if moves is None:
                    break
                    # If for some reason your king is surrounded. I need to put in some kind of marker for stalemates
        # Castling
        if not self.has_moved:
            for move in self.position.count_right():
                if board.occupied(move):
                    if board[move].is_rook() and self.is_white() == board[move].is_white():
                        moves.append(Coord(7, self.get_y()))
                        break
                    else: break

            for move in self.position.count_left():
                if board.occupied(move):
                    if board[move].is_rook() and self.is_white() == board[move].is_white():
                        moves.append(Coord(2, self.get_y()))
                        break
                    else: break

        return moves

class NoPiece(ChessPiece):
    def __init__(self, color, image):
        self.color = color
        self.image = image.image
        self.rect = image.rect
        self.position = None