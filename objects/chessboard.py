from objects.coordinate import Coordinate as Coord


# TODO Refactor moves to an 8x8 board with can_attack flags for each piece
class Chessboard:
    # Parent class for the past, present, and future chessboards
    # Alternatively, use this if you want to play a normal game of chess
    def __init__(self, image):
        self.width = 8
        self.height = 8
        self.image = image.image.convert_alpha()
        self.rect = image.rect
        self.left_axis = tuple(range(1, 9))
        self.top_axis = tuple(range(1, 9))
        self.contents = {}
        # These two lists are for checking threats
        self.black_pieces = []
        self.white_pieces = []

        for x in self.top_axis:
            for y in self.left_axis:
                self.contents[(x, y)] = None
        # White has bottom, black has top

    def occupied(self, location):
        if self.contents[location].is_none():
            return False
        else:
            return True


    def center(self, screen):
        self.rect.move_ip(screen.get_width()//2 - self.rect.width//2, screen.get_height()//2 - self.rect.height//2)

    def draw(self, screen):
        screen.blit(self.image, self.rect)
        for item in self.contents:
            piece = self.contents[item]
            if piece is not None:
                piece.draw(screen)


    def populate(self, pieces):
        pawn_white_count = 1
        pawn_black_count = 1
        blank_square_x = 1
        blank_square_y = 3
        for piece in pieces:
            if piece.is_pawn():
                if piece.is_white():
                    self.contents[Coord(pawn_white_count, 2)] = piece
                    piece.position = Coord(pawn_white_count, 2)
                    pawn_white_count += 1
                else:
                    self.contents[Coord(pawn_black_count, 7)] = piece
                    piece.position = Coord(pawn_black_count, 7)
                    pawn_black_count += 1

            elif piece.is_rook():
                if piece.is_white():
                    if self.contents[Coord(1, 1)] is None:
                        self.contents[Coord(1, 1)] = piece
                        piece.position = Coord(1, 1)
                    else:
                        self.contents[Coord(8, 1)] = piece
                        piece.position = Coord(8, 1)
                else:
                    if self.contents[Coord(1, 8)] is None:
                        self.contents[Coord(1, 8)] = piece
                        piece.position = Coord(1, 8)
                    else:
                        self.contents[Coord(8, 8)] = piece
                        piece.position = Coord(8, 8)
            elif piece.is_knight():
                if piece.is_white():
                    if self.contents[Coord(2, 1)] is None:
                        self.contents[Coord(2, 1)] = piece
                        piece.position = Coord(2, 1)
                    else:
                        self.contents[Coord(7, 1)] = piece
                        piece.position = Coord(7, 1)
                else:
                    if self.contents[Coord(2, 8)] is None:
                        self.contents[Coord(2,  8)] = piece
                        piece.position = Coord(2, 8)
                    else:
                        self.contents[Coord(7, 8)] = piece
                        piece.position = Coord(7, 8)
            elif piece.is_bishop():
                if piece.is_white():
                    if self.contents[Coord(3, 1)] is None:
                        self.contents[Coord(3, 1)] = piece
                        piece.position = Coord(3, 1)
                    else:
                        self.contents[Coord(6, 1)] = piece
                        piece.position = Coord(6, 1)
                else:
                    if self.contents[Coord(3, 8)] is None:
                        self.contents[Coord(3, 8)] = piece
                        piece.position = Coord(3, 8)
                    else:
                        self.contents[Coord(6, 8)] = piece
                        piece.position = Coord(6, 8)
            elif piece.is_queen():
                if piece.is_white():
                    if self.contents[Coord(5, 1)] is None:
                        self.contents[Coord(5, 1)] = piece
                        piece.position = Coord(5, 1)
                else:
                    if self.contents[Coord(5, 8)] is None:
                        self.contents[Coord(5, 8)] = piece
                        piece.position = Coord(5, 8)
            elif piece.is_king():
                if piece.is_white():
                    if self.contents[Coord(4, 1)] is None:
                        self.contents[Coord(4, 1)] = piece
                        piece.position = Coord(4, 1)
                else:
                    if self.contents[Coord(4, 8)] is None:
                        self.contents[Coord(4, 8)] = piece
                        piece.position = Coord(4, 8)
            elif piece.is_none():
                if self.contents[Coord(blank_square_x, blank_square_y)] is None:
                    self.contents[Coord(blank_square_x, blank_square_y)] = piece
                    piece.position = Coord(blank_square_x, blank_square_y)
                    blank_square_x += 1
                    if blank_square_x == 9:
                        blank_square_y += 1
                        blank_square_x = 1

        for piece in pieces:
            if not piece.is_none():
                if piece.is_white():
                    if not piece.is_king():
                        self.white_pieces.append(piece)
                else:
                    if not piece.is_king():
                        self.black_pieces.append(piece)

        self.set_piece_rects()

    def depopulate(self):
        for item in self.contents:
            piece = self.contents[item]
            if piece is not None:
                self.contents[item] = None

    def set_piece_rects(self):
        origin_point = self.rect.topleft[0] + 18 - 84, self.rect.topleft[1] + 18 - 84
        for item in self.contents:
            piece = self.contents[item]
            if piece is not None:
                piece.rect.topleft = origin_point
                piece.rect.move_ip(piece.position[0] * 84, piece.position[1] * 84)

    def __repr__(self):
        display_list = []
        for x in self.top_axis:
            for y in self.left_axis:
                display_list.append(str((x, y)))
                display_list.append(str(self.contents[(x, y)]))
        return str(display_list)

    def __getitem__(self, item):
        return self.contents[item]

    def __setitem__(self, key, value):
        self.contents[key] = value
