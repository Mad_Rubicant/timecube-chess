from objects import chess_piece as CP
import art.load

def initialize_pieces():
    # Start with the pawns
    pieces = []
    images = art.load.load_piece_images()
    white = images[0]
    black = images[1]
    for i in range(8):
        pieces.append(CP.Pawn(0, art.load.Image(white[3])))
        pieces.append(CP.Pawn(1, art.load.Image(black[3])))

    # Rooks
    for i in range(2):
        pieces.append(CP.Rook(0, art.load.Image(white[5])))
        pieces.append(CP.Rook(1, art.load.Image(black[5])))
    # Bishops
    for i in range(2):
        pieces.append(CP.Bishop(0, art.load.Image(white[0])))
        pieces.append(CP.Bishop(1, art.load.Image(black[0])))
    # Knights
    for i in range(2):
        pieces.append(CP.Knight(0, art.load.Image(white[2])))
        pieces.append(CP.Knight(1, art.load.Image(black[2])))
    # Queen
    pieces.append(CP.Queen(0, art.load.Image(white[4])))
    pieces.append(CP.Queen(1, art.load.Image(black[4])))
    # King
    pieces.append(CP.King(0, art.load.Image(white[1])))
    pieces.append(CP.King(1, art.load.Image(black[1])))

    for i in range(32):
        pieces.append((CP.NoPiece(None, art.load.load_blank())))

    return pieces

def new_blank(position):
    image = art.load.load_blank()
    blank = CP.NoPiece(None, image)
    blank.position = position
    return blank